package main;

/**
 * Das System rennt in einem Thread und ruft immer Regelbar auf.
 */
public class System implements Runnable {

	/**
	 * Attribute
	 */
	private int mSysId;
	private final Systemstartattribute mAttribute;
	private Datensammler mDatenmelder;
	private double[] mSystemzustand;
	private final RungenKutta mRungenKutta;
	private final Regelbar mEingang;
	private double mMaxX;
	private double mCurrentX;
	private double mSchrittweite;
	private volatile Thread laufSystemLauf;
	private long mZaehler = 0;
	private int mAbtastFaktor = 0;

	public System(int pSysId, Systemstartattribute pSysAttr,
			Datensammler pDatenmelder, DerivnFunction pFunc, Regelbar pEingang) {
		this.mSysId = pSysId;
		this.mAttribute = pSysAttr;
		this.mDatenmelder = pDatenmelder;
		this.mSystemzustand = pSysAttr.getStartY();
		this.mMaxX = pSysAttr.getMaxX();
		this.mCurrentX = pSysAttr.getStartX();
		this.mSchrittweite = pSysAttr.getSchrittweite();
		this.mAbtastFaktor = pSysAttr.getAbtastfaktor();
		this.mRungenKutta = new RungenKutta(mSchrittweite, pFunc);
		if (pEingang == null) {
			mEingang = new Regler(pSysAttr.getP(), pSysAttr.getI(),
					pSysAttr.getD(), pSysAttr.getZiel(), mSchrittweite,
					pSysAttr.getMinEingang(), pSysAttr.getMaxEingang());
		} else {
			mEingang = pEingang;
		}
	}

	public void start() {
		laufSystemLauf = new Thread(this);
		laufSystemLauf.start();
	}

	public void stop() {
		laufSystemLauf = null;
	}

	@Override
	public void run() {
		Thread thisThread = Thread.currentThread();
		while (laufSystemLauf == thisThread) {
			double newX = mCurrentX + mSchrittweite;
			if (newX < mMaxX) {
				// Eingang anstossen:
				if (mAbtastFaktor != 0) {
					if (mZaehler % mAbtastFaktor == 0) {
						mEingang.reglerInput(mSystemzustand, mCurrentX);
					}
				} else {
					mEingang.reglerInput(mSystemzustand, mCurrentX);
				}
				mSystemzustand = mRungenKutta.fourthOrder(mCurrentX,
						mSystemzustand, mEingang.reglerOutput());
				mDatenmelder.aktuellerSystemzustand(mSysId, mSystemzustand,
						newX);
				mDatenmelder.aktuellerRegelbarzustand(mSysId,
						mEingang.reglerOutput(), newX);
				try {
					Thread.sleep(mAttribute.getSystemRechenzeit());
				} catch (InterruptedException e) {
					// We got interrupted, so what?
					laufSystemLauf = null;
					return;
				}
			} else {
				laufSystemLauf = null;
				return;
			}
			mZaehler++;
			mCurrentX = newX;
		}
	}

	private class RungenKutta {
		private double mH;
		private DerivnFunction mFunc;

		public RungenKutta(double pSchrittweite, DerivnFunction pFunc) {
			this.mH = pSchrittweite;
			this.mFunc = pFunc;
		}

		public double[] fourthOrder(double pX, double[] pY, double[] pU) {
			int nODE = pY.length;
			double[] k1 = new double[nODE];
			double[] k2 = new double[nODE];
			double[] k3 = new double[nODE];
			double[] k4 = new double[nODE];
			double[] y = new double[nODE];
			double[] yd = new double[nODE];
			// initialise
			for (int i = 0; i < nODE; i++)
				y[i] = pY[i];
			// RungeKutta
			// k1
			k1 = mFunc.derivn(pX, y, pU);
			// k2
			for (int i = 0; i < nODE; i++)
				yd[i] = y[i] + (mH / 2) * k1[i];
			k2 = mFunc.derivn(pX + (mH / 2), yd, pU);
			// k3
			for (int i = 0; i < nODE; i++)
				yd[i] = y[i] + (mH / 2) * k2[i];
			k3 = mFunc.derivn(pX + (mH / 2), yd, pU);
			// k4
			for (int i = 0; i < nODE; i++)
				yd[i] = y[i] + mH * k3[i];
			k4 = mFunc.derivn(pX + mH, yd, pU);
			// y
			for (int i = 0; i < nODE; i++)
				y[i] += k1[i] / 6 + k2[i] / 3 + k3[i] / 3 + k4[i] / 6;
			return y;
		}
	}
}
