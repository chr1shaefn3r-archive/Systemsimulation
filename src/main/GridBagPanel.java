package main;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JPanel;

public class GridBagPanel extends JPanel {
	private static final long serialVersionUID = 3209414940597396641L;

	private GridBagConstraints gb;

	/**
	 * Konstruktor - Initialisierung
	 */
	public GridBagPanel() {
		super(new GridBagLayout());
		gb = new GridBagConstraints();

		gb.gridx = 0;
		gb.gridy = 0;
		gb.weightx = 0;
		gb.weighty = 0;
		gb.ipadx = 0;
		gb.ipady = 0;
		gb.anchor = GridBagConstraints.NORTHWEST;
		gb.insets = new Insets(5, 5, 5, 5);
	}

	/**
	 * Gibt die GridBagContraints zurueck
	 * 
	 * @return
	 */
	public GridBagConstraints getGridBagConstraints() {
		return this.gb;
	}

	/**
	 * Fuegt element hinzu
	 * 
	 * @param c
	 */
	public void add2Column(JComponent c) {
		this.add2Column(c, 1);
	}

	/**
	 * Setzt gewuenschte x-Gewichtung
	 * @param weightX
	 */
	public void setWeightFill(double weightX) {
		setWeightFill(weightX, 0);
	}
	
	/**
	 * Setzt die gewuenschte Gewichtung
	 * 
	 * @param weightX
	 * @param weightY
	 */
	public void setWeightFill(double weightX, double weightY) {
		this.gb.weightx = weightX;
		this.gb.weighty = weightY;
		if (weightX != 0) {
			if (weightY != 0) {
				this.gb.fill = GridBagConstraints.BOTH;
			} else {
				this.gb.fill = GridBagConstraints.HORIZONTAL;
			}
		} else if (weightY != 0) {
			this.gb.fill = GridBagConstraints.VERTICAL;
		}
	}

	/**
	 * Setzt gewuenschte Ausrichtung
	 * 
	 * @param align
	 */
	public void setAlign(String align) {
		if (align.equals("right")) {
			this.gb.anchor = GridBagConstraints.EAST;
		} else {
			this.gb.anchor = GridBagConstraints.NORTHWEST;
		}
	}

	/**
	 * Setzt die gewuenschte Breite und fuegt Element hinzu
	 * 
	 * @param c
	 * @param gridwidth
	 */
	public void add2Column(JComponent c, int gridwidth) {
		this.gb.gridwidth = gridwidth;
		this.add(c, this.gb);
		this.gb.gridx++;

		// WeightFill zuruecksetzten
		this.gb.weightx = 0;
		this.gb.weighty = 0;
		this.gb.fill = GridBagConstraints.NONE;

		// Align zuruecksetzten
		setAlign("");
	}

	/**
	 * Starte eine neue Reihe
	 */
	public void nextRow() {
		this.gb.gridx = 0;
		this.gb.gridy++;
	}
}