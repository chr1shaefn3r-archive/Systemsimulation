package main;

public interface Datensammler {
	public void aktuellerSystemzustand(int pSysId, double[] pSystemzustand, double pZeit);
	public void aktuellerRegelbarzustand(int pSysId, double[] pAusgang, double pZeit);
}