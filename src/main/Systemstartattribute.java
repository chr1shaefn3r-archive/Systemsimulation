package main;

public class Systemstartattribute {

	/**
	 * Attribute
	 */
	/**
	 * Der Faktor um wie viel "langsamer" der Regler rechnen soll
	 */
	private double mStartX;
	private double mMaxX;
	private double mSchrittweite;
	private double[] mStartY;
	private long mSystemRechenzeit = 50;
	private String mSystemDefinition;
	private String mEingangsDefinition;
	private boolean mReglerCheck;
	private double mZiel;
	private int mAbtastfaktor = 0;
	private double mMinimalerEingang;
	private double mMaximalerEingang;
	private double mP;
	private double mI;
	private double mD;

	public Systemstartattribute(String pSystemDefinition,
			String pEingangsDefinition, double pStartX, double pMaxX,
			double pSchrittweite, double[] pStartY, boolean pReglerCheck,
			double pZiel, int pAbtastfaktor, double pMinEingang,
			double pMaxEingang, double pP, double pI, double pD) {
		this.mSystemDefinition = pSystemDefinition;
		this.mEingangsDefinition = pEingangsDefinition;
		this.mStartX = pStartX;
		this.mMaxX = pMaxX;
		this.mSchrittweite = pSchrittweite;
		this.mStartY = pStartY;
		this.mReglerCheck = pReglerCheck;
		this.mZiel = pZiel;
		this.mAbtastfaktor = pAbtastfaktor;
		this.mMinimalerEingang = pMinEingang;
		this.mMaximalerEingang = pMaxEingang;
		this.mP = pP;
		this.mI = pI;
		this.mD = pD;
	}

	public String getSystemDefinition() {
		return mSystemDefinition;
	}

	public String getEingangsDefinition() {
		return mEingangsDefinition;
	}

	public long getSystemRechenzeit() {
		return mSystemRechenzeit;
	}

	public double getStartX() {
		return mStartX;
	}

	public double getMaxX() {
		return mMaxX;
	}

	public double getSchrittweite() {
		return mSchrittweite;
	}

	public double[] getStartY() {
		return mStartY;
	}

	public boolean getReglerCheck() {
		return mReglerCheck;
	}

	public double getZiel() {
		return mZiel;
	}

	public int getAbtastfaktor() {
		return mAbtastfaktor;
	}

	public double getMinEingang() {
		return mMinimalerEingang;
	}

	public double getMaxEingang() {
		return mMaximalerEingang;
	}

	public double getP() {
		return mP;
	}

	public double getI() {
		return mI;
	}

	public double getD() {
		return mD;
	}

}
