package main;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

import main.GUI.GUIEvents;
import ptolemy.plot.Plot;

public class SysPlot extends Plot {
	private static final long serialVersionUID = 5885851132072772414L;

	/**
	 * Attribute
	 */
	private int mLegendOffset;
	private int mLegendCount;
	private boolean mConnectPoints;
	private final int mPlotID;
	private double mPlotId;
	private JFrame mPlotFrame;
	private final  GUIEvents mGuiEvent;
	
	/**
	 * Konstruktor
	 */
	
	public SysPlot(GUIEvents pEvt, int pID) {
		this(pEvt, pID, 0.0, 10.0, -10.0, 10.0);
	}
	
	/**
	 * 
	 * @param pID PlotID = SystemID
	 * @param pMinX Minimum X
	 * @param pMaxX Maximum X
	 * @param pStepsX Skalierung in Steps in X
	 * @param pTextX Abszissenbeschriftung
	 * @param pMinY Minimum Y
	 * @param pMaxY Maximum Y
	 * @param pStepsY Skalierung in Steps in Y
	 * @param pTextY Ordinatenbeschriftung
	 * @param pTitel Titel des Plots
	 */
	public SysPlot(GUIEvents pEvt, int pID, double pMinX, double pMaxX, double pMinY, double pMaxY){
		this.setXRange(pMinX, pMaxX);
		this.setYRange(pMinY, pMaxY);
		this.mPlotID = pID;
		this.mGuiEvent = pEvt;
		this.mLegendOffset = 0;
		this.mLegendCount = 0;
		this.createFrame();
		this.mConnectPoints = true;
	}

	/**
	 * Erzeuge ein Fenster auf dem sich der Plot befindet
	 */
	private void createFrame() {
		this.mPlotFrame = new JFrame(this.getTitle());
		this.mPlotFrame.addWindowListener(new WindowListener(){

			@Override
			public void windowActivated(WindowEvent arg0) {}

			@Override
			public void windowClosed(WindowEvent arg0) {}

			@Override
			public void windowClosing(WindowEvent arg0) {
				SysPlot.this.mGuiEvent.plotfensterClosed(SysPlot.this.mPlotID);
				SysPlot.this.mPlotFrame.dispose();
			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {}

			@Override
			public void windowDeiconified(WindowEvent arg0) {}

			@Override
			public void windowIconified(WindowEvent arg0) {}

			@Override
			public void windowOpened(WindowEvent arg0) {}
			
		});
		this.mPlotFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.mPlotFrame.add(this);
		this.mPlotFrame.pack();
		this.mPlotFrame.setVisible(true);
	}
	
	/**
	 * Methoden zum plotten einzelner Punkte oder Vektoren
	 */
	
	public void addVectorPointsSystem(double pXPoint, double pSkalarY){
		this.addVectorPointsSystem(pXPoint, new double[]{pSkalarY});
	}
	
	public void addVectorPointsRegler(double pXPoint, double pSkalarY){
		this.addVectorPointsRegler(pXPoint, new double[]{pSkalarY});
	}
	
	public void addVectorPointsSystem(double pXPoint, double[] pVectorY){
		for(int i=0; i<pVectorY.length; i++){
			java.lang.System.out.println("[SysPlot] this.addPoint("+i+", "+pXPoint+", "+pVectorY[i]+", "+this.mConnectPoints+")");
			this.addPoint(i, pXPoint, pVectorY[i], this.mConnectPoints);
		}
	}
	
	public void addVectorPointsRegler(double pXPoint, double[] pVectorY){
		for(int i=0; i<pVectorY.length; i++){
			this.addPoint(i+this.mLegendOffset, pXPoint, pVectorY[i], this.mConnectPoints);
		}
	}
	
	/**
	 * Methoden zum hinzufuegen von Legenden
	 */
	
	public void setLegende(String[] pSystem, String[] pRegler, String pXAchse, String pYAchse, String pTitel){
		//Entferne alte Funktionslegenden
		for(int i=0; i<this.mLegendCount; i++){
			this.removeLegend(i);
		}
		
		//Anzahl und Offset der Legenden [System|Regler]
		this.mLegendCount = pSystem.length + pRegler.length;
		this.mLegendOffset = pSystem.length;
		
		//Fuege neue Legende hinzu
		for(int i=0; i<pSystem.length; i++){
			this.addLegend(i, pSystem[i]);
		}
		for(int i=0; i<pRegler.length; i++){
			this.addLegend(i+this.mLegendOffset, pRegler[i]);
		}
		//Achsenbeschriftung
		this.setXLabel(pXAchse);
		this.setYLabel(pYAchse);
		this.setTitle(pTitel);
	}
	
	/**
	 * Methoden zum Refreshen eines Plots
	 */
	public void refreshPlot(){
	}
	
	/**
	 * GETTERS UND SETTERS
	 */
	
	public boolean ismConnectPoints() {
		return mConnectPoints;
	}

	public void setmConnectPoints(boolean mConnectPoints) {
		this.mConnectPoints = mConnectPoints;
	}

	public double getmPlotId() {
		return mPlotId;
	}

	public void setmPlotId(double mPlotId) {
		this.mPlotId = mPlotId;
	}

	public JFrame getmPlotFrame() {
		return mPlotFrame;
	}

	public int getmPlotID() {
		return mPlotID;
	}
}

