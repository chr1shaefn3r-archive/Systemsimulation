package main;

/**
 * Diese Klasse stellt einen einfachen Eingang (algebraische Gleichung) dar.
 */
public class Eingang implements Regelbar {

	private double mZeit;
	/**
	 * Wird bei einem einfachen (fast) Eingang ignoriert
	 */
	@Override
	public void reglerInput(double[] pSystemzustand, double pZeit) {
		this.mZeit = pZeit;
	}

	/**
	 * Wird durch eine algebraische Funktion implementiert
	 */
	@Override
	public double[] reglerOutput() {
		double[] ergebnis = new double[]{Math.sin(mZeit)};
		return ergebnis;
	}

}
