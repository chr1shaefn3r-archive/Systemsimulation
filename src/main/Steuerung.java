package main;

import java.io.IOException;
import java.util.ArrayList;

import main.GUI.GUIEvents;


public class Steuerung implements GUIEvents, Datensammler {
	/**
	 * Attribute
	 */
	private final DerivnFunctionInstanziierer mDerivnFunctionInstanziierer;
	private final RegelbarInstanziierer mRegelbarInstanziierer;
	private final ArrayList<System> mSysteme = new ArrayList<System>();
	private final ArrayList<SysPlot> mPlotFenster = new ArrayList<SysPlot>();
	
	/**
	 * Konstruktor
	 */
	public Steuerung(){
		new GUI(this);
		this.mDerivnFunctionInstanziierer = new DerivnFunctionInstanziierer();
		this.mRegelbarInstanziierer = new RegelbarInstanziierer();
	}

	/**
	 * Main
	 */
	public static void main(String[] args) {
		new Steuerung();
	}

	@Override
	public void clickStart(Systemstartattribute pSystemzustand) {
		DerivnFunction function = null;
		Regelbar regelbar = null;
		try {
			function = mDerivnFunctionInstanziierer.instanziiere(pSystemzustand.getSystemDefinition());
		} catch (IOException e) {
			e.printStackTrace();
		}
		java.lang.System.out.println("[Steuerung] pSystemzustand.getReglerCheck(): "+pSystemzustand.getReglerCheck());
		if(/*this.isNotEmpty(pSystemzustand.getEingangsDefinition()) && */!pSystemzustand.getReglerCheck()) {
			try {
				regelbar = mRegelbarInstanziierer.instanziiere(pSystemzustand.getEingangsDefinition());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System system = new System(mSysteme.size(), pSystemzustand, this, function, regelbar);
		mSysteme.add(system);
		SysPlot plot = new SysPlot(this, mPlotFenster.size(), pSystemzustand.getStartX(), pSystemzustand.getMaxX(), 0, pSystemzustand.getZiel()+10);
		plot.setLegende(new String[]{"h1", "h2"}, new String[]{"u"}, "t", "m", "Zwei-Tank-System");
		mPlotFenster.add(plot);
		system.start();
	}

	@Override
	public void plotfensterClosed(int pSysId) {
		mSysteme.get(pSysId).stop();
		mSysteme.set(pSysId, null);
	}

	@Override
	public void aktuellerSystemzustand(int pSysId, double[] pSystemzustand, double pZeit) {
		mPlotFenster.get(pSysId).addVectorPointsSystem(pZeit, pSystemzustand);

	}

	@Override
	public synchronized void aktuellerRegelbarzustand(int pSysId, double[] pAusgang, double pZeit) {
		mPlotFenster.get(pSysId).addVectorPointsRegler(pZeit, pAusgang);
	}

	private boolean isEmpty(String pText) {
		if(pText == null) return false;
		else return (pText.length() < 0);
	}

	private boolean isNotEmpty(String pText) {
		return !isEmpty(pText);
	}
}
