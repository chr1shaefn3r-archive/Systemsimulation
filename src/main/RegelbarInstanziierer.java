package main;

import java.io.IOException;

public class RegelbarInstanziierer extends Instanziierer {

	public RegelbarInstanziierer() {
		super("RungenKutta", "Regelbar");
	}

	public Regelbar instanziiere(String pClass) throws IOException{
		return (Regelbar)super.instanziiere(pClass);
	}
}