package main;

public interface Regelbar {
	public void reglerInput(double[] pSystemzustand, double pZeit);
	public double[] reglerOutput();
}