package main;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class GUI extends JFrame {
	private static final long serialVersionUID = 632166259858157042L;

	/**
	 * Interface
	 */
	public interface GUIEvents {
		public void clickStart(Systemstartattribute pSystemzustand);

		public void plotfensterClosed(int pSysId);
	}

	/**
	 * Attribute
	 */
	private static final int SLD_P = 0, SLD_I = 0, SLD_D = 0, SLD_MIN = 0,
			SLD_MAX = 100;
	private static final double DOUBLE_NEUTRAL = 0.0;
	private static final int INTEGER_NEUTRAL = 0;
	private static final int MAX_INT_SIZE = 10;
	private final GUIEvents mCallback;

	/**
	 * 
	 * @param s
	 *            : Steuerung
	 */
	public GUI(GUIEvents pCallback) {
		this.mCallback = pCallback;
		this.createGUI();
	}

	/**
	 * Methoden
	 */
	private void createGUI() {
		/*
		 * Elemente initialisieren
		 */
		final GridBagPanel gbp_main = new GridBagPanel(), gbp_sys = new GridBagPanel(), gbp_par = new GridBagPanel(), gbp_ein = new GridBagPanel(), gbp_reg = new GridBagPanel(), gbp_reg_sld = new GridBagPanel(), gbp_reg_txt = new GridBagPanel();
		final JCheckBox cb_main_reg;
		JButton but_main_start;

		JLabel lbl_sys, lbl_par, lbl_par_x0, lbl_par_xMax, lbl_par_h, lbl_par_y0;
		final JTextArea txt_sys, txt_par_y0;
		final JTextField txt_par_x0, txt_par_xMax, txt_par_h;
		JScrollPane txt_sys_scroll, txt_par_y0_scroll;

		JLabel lbl_ein;
		final JTextArea txt_ein;
		JScrollPane txt_ein_scroll;

		JLabel lbl_reg, lbl_reg_p, lbl_reg_i, lbl_reg_d, lbl_reg_z, lbl_reg_t, lbl_reg_uMin, lbl_reg_uMax;
		final JLabel lbl_reg_pZ, lbl_reg_iZ, lbl_reg_dZ;
		final JTextField txt_reg_z, txt_reg_t, txt_reg_uMin, txt_reg_uMax;
		final JSlider sld_reg_p, sld_reg_i, sld_reg_d;

		/*
		 * INIT Label
		 */
		lbl_sys = new JLabel("System");
		lbl_sys.setOpaque(true);

		lbl_par = new JLabel("Parameter");
		lbl_par.setOpaque(true);
		lbl_par_x0 = new JLabel("<html>x<sub>0</sub></html>");
		lbl_par_x0.setOpaque(true);
		lbl_par_xMax = new JLabel("<html>x<sub>max</sub></html>");
		lbl_par_xMax.setOpaque(true);
		lbl_par_h = new JLabel("h");
		lbl_par_h.setOpaque(true);
		lbl_par_y0 = new JLabel("<html>y<sub>0</sub></html>");
		lbl_par_y0.setOpaque(true);

		lbl_ein = new JLabel("Eingang");
		lbl_ein.setOpaque(true);

		lbl_reg = new JLabel("Regler");
		lbl_reg.setOpaque(true);
		lbl_reg_p = new JLabel("P");
		lbl_reg_p.setOpaque(true);
		lbl_reg_i = new JLabel("I");
		lbl_reg_i.setOpaque(true);
		lbl_reg_d = new JLabel("D");
		lbl_reg_d.setOpaque(true);
		lbl_reg_pZ = new JLabel(String.valueOf(SLD_P));
		lbl_reg_pZ.setPreferredSize(new Dimension(25, 15));
		lbl_reg_pZ.setOpaque(true);
		lbl_reg_iZ = new JLabel(String.valueOf(SLD_I));
		lbl_reg_iZ.setPreferredSize(new Dimension(25, 15));
		lbl_reg_iZ.setOpaque(true);
		lbl_reg_dZ = new JLabel(String.valueOf(SLD_D));
		lbl_reg_dZ.setPreferredSize(new Dimension(25, 15));
		lbl_reg_dZ.setOpaque(true);
		lbl_reg_z = new JLabel("Ziel");
		lbl_reg_z.setOpaque(true);
		lbl_reg_t = new JLabel("T");
		lbl_reg_t.setOpaque(true);
		lbl_reg_uMin = new JLabel("<html>u<sub>min</sub></html>");
		lbl_reg_uMin.setOpaque(true);
		lbl_reg_uMax = new JLabel("<html>u<sub>max</sub></html>");
		lbl_reg_uMax.setOpaque(true);

		/*
		 * INIT TextArea
		 */
		txt_sys = new JTextArea(
				"package main;\n"
						+ "import main.DerivnFunction;\n"
						+ "class RungenKutta implements DerivnFunction {\n"
						+ "@Override\n"
						+ "public double[] derivn(double x, double[] y, double[] u) {\n"

						+ "double[] ergebnis = new double[2];\n"
						+ "/* A1 = A2 = 1\n"
						+ "* k = sqrt(2*g) mit g=9.81\n"
						+ "* h1. = -(k/A1)*Math.sqrt(h1) + (1/A1)*q0\n"
						+ "* h2. = (k/A2)*Math.sqrt(h1)-(k/A2)*Math.sqrt(h2)\n"
						+ "*/\n"
						+ "double a1 = 10, a2 = 13;\n"
						+ "double k = Math.sqrt(2*9.81);\n"
						+ "ergebnis[0] = -(k/a1)*Math.sqrt(y[0])+(1/a1)*u[0];\n"
						+ "ergebnis[1] = (k/a2)*Math.sqrt(y[0])-(k/a2)*Math.sqrt(y[1]);\n"
						+ "return ergebnis;\n" + "}\n" + "}");
		// + "double[] ergebnis = new double[1];\n" + "/* A = 1\n"
		// + "* h. = A*u(t)\n" + "*/\n" + "int a = 1;\n"
		// + "ergebnis[0] = a*u[0];\n" + "return ergebnis;\n"
		// + "}\n" + "}");
		txt_sys.setOpaque(true);

		txt_par_y0 = new JTextArea("21.0\n11.0");
		txt_par_y0.setOpaque(true);
		setCheckEingabe(txt_par_y0);

		txt_ein = new JTextArea(
				"package main;\n"
						+ "import main.Regelbar;\n"
						+ "class RungenKutta implements Regelbar {\n"
						+ "private double mZeit;"
						+ "public void reglerInput(double[] pSystemzustand, double pZeit) {\n"
						+ "	this.mZeit = pZeit;\n" + "}\n"
						+ "public double[] reglerOutput() {\n"
						+ "	double[] ergebnise = new double[]{10};\n"
						+ "return ergebnise;\n" + "}\n" + "}");
		txt_ein.setOpaque(true);

		/*
		 * INIT ScrollPane
		 */
		txt_sys_scroll = new JScrollPane(txt_sys);
		txt_sys_scroll
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		txt_par_y0_scroll = new JScrollPane(txt_par_y0);
		txt_par_y0_scroll
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		txt_ein_scroll = new JScrollPane(txt_ein);
		txt_ein_scroll
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		/*
		 * INIT TextField
		 */
		txt_par_x0 = new JTextField(String.valueOf(Double
				.valueOf(DOUBLE_NEUTRAL)));
		setCheckEingabe(txt_par_x0);
		txt_par_xMax = new JTextField(String.valueOf(Double.valueOf("1000")));
		setCheckEingabe(txt_par_xMax);
		txt_par_h = new JTextField(String.valueOf(Double.valueOf("1")));
		setCheckEingabe(txt_par_h);

		txt_reg_z = new JTextField(String.valueOf(Double.valueOf("42")));
		setCheckEingabe(txt_reg_z);
		txt_reg_t = new JTextField(String.valueOf("5"));
		setCheckEingabe(txt_reg_t, "int");
		txt_reg_uMin = new JTextField(String.valueOf(Double
				.valueOf(DOUBLE_NEUTRAL)));
		setCheckEingabe(txt_reg_uMin);
		txt_reg_uMax = new JTextField(String.valueOf(Double.valueOf("100")));
		setCheckEingabe(txt_reg_uMax);

		/*
		 * INIT Slider
		 */
		sld_reg_p = new JSlider(JSlider.HORIZONTAL, SLD_MIN, SLD_MAX, SLD_P);
		sld_reg_p.setMajorTickSpacing(20);
		sld_reg_p.setMinorTickSpacing(5);
		sld_reg_p.setPaintTicks(true);
		sld_reg_p.setPaintLabels(true);
		sld_reg_p.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				lbl_reg_pZ.setText(String.valueOf(sld_reg_p.getValue()));
			}
		});
		sld_reg_p.setValue(1);
		sld_reg_i = new JSlider(JSlider.HORIZONTAL, SLD_MIN, SLD_MAX, SLD_I);
		sld_reg_i.setMajorTickSpacing(20);
		sld_reg_i.setMinorTickSpacing(5);
		sld_reg_i.setPaintTicks(true);
		sld_reg_i.setPaintLabels(true);
		sld_reg_i.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				lbl_reg_iZ.setText(String.valueOf(sld_reg_i.getValue()));
			}
		});
		sld_reg_i.setValue(1);
		sld_reg_d = new JSlider(JSlider.HORIZONTAL, SLD_MIN, SLD_MAX, SLD_D);
		sld_reg_d.setMajorTickSpacing(20);
		sld_reg_d.setMinorTickSpacing(5);
		sld_reg_d.setPaintTicks(true);
		sld_reg_d.setPaintLabels(true);
		sld_reg_d.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				lbl_reg_dZ.setText(String.valueOf(sld_reg_d.getValue()));
			}
		});

		/*
		 * INIT CheckBox
		 */
		cb_main_reg = new JCheckBox("Regler");
		cb_main_reg.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (gbp_ein.isVisible()) {
					gbp_ein.setVisible(false);
					gbp_reg.setVisible(true);
				} else {
					gbp_ein.setVisible(true);
					gbp_reg.setVisible(false);
				}
			}
		});

		/*
		 * INIT Button
		 */
		but_main_start = new JButton("Start");
		but_main_start.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				/*
				 * Inputfelder auslesen
				 */
				Systemstartattribute systemStartattribute = new Systemstartattribute(
						txt_sys.getText(), txt_ein.getText(),
						getDouble(txt_par_x0), getDouble(txt_par_xMax),
						getDouble(txt_par_h), getDouble(txt_par_y0),
						cb_main_reg.isSelected(), getDouble(txt_reg_z),
						getInteger(txt_reg_t), getDouble(txt_reg_uMin),
						getDouble(txt_reg_uMax), getDouble(lbl_reg_pZ),
						getDouble(lbl_reg_iZ), getDouble(lbl_reg_dZ));
				mCallback.clickStart(systemStartattribute);
			}
		});

		/*
		 * Pannel mit Elementen bestuecken
		 */
		gbp_sys.add2Column(lbl_sys);
		gbp_sys.nextRow();
		gbp_sys.setWeightFill(1, 1);
		gbp_sys.add2Column(txt_sys_scroll);

		gbp_par.add2Column(lbl_par, 2);
		gbp_par.nextRow();
		setElementSize(lbl_par_x0);
		gbp_par.add2Column(lbl_par_x0);
		setElementSize(txt_par_x0);
		gbp_par.add2Column(txt_par_x0);
		gbp_par.nextRow();
		setElementSize(lbl_par_xMax);
		gbp_par.add2Column(lbl_par_xMax);
		setElementSize(txt_par_xMax);
		gbp_par.add2Column(txt_par_xMax);
		gbp_par.nextRow();
		setElementSize(lbl_par_h);
		gbp_par.add2Column(lbl_par_h);
		setElementSize(txt_par_h);
		gbp_par.add2Column(txt_par_h);
		gbp_par.nextRow();
		setElementSize(lbl_par_y0);
		gbp_par.add2Column(lbl_par_y0);
		setElementSize(txt_par_y0_scroll);
		gbp_par.add2Column(txt_par_y0_scroll);

		gbp_ein.add2Column(lbl_ein);
		gbp_ein.nextRow();
		gbp_ein.setWeightFill(1, 1);
		gbp_ein.add2Column(txt_ein_scroll);

		gbp_reg_sld.add2Column(lbl_reg, 3);
		gbp_reg_sld.nextRow();
		gbp_reg_sld.add2Column(lbl_reg_p);
		gbp_reg_sld.add2Column(lbl_reg_pZ);
		gbp_reg_sld.setWeightFill(1);
		gbp_reg_sld.add2Column(sld_reg_p);
		gbp_reg_sld.nextRow();
		gbp_reg_sld.add2Column(lbl_reg_i);
		gbp_reg_sld.add2Column(lbl_reg_iZ);
		gbp_reg_sld.setWeightFill(1);
		gbp_reg_sld.add2Column(sld_reg_i);
		gbp_reg_sld.nextRow();
		gbp_reg_sld.add2Column(lbl_reg_d);
		gbp_reg_sld.add2Column(lbl_reg_dZ);
		gbp_reg_sld.setWeightFill(1);
		gbp_reg_sld.add2Column(sld_reg_d);

		setElementSize(lbl_reg_z);
		gbp_reg_txt.add2Column(lbl_reg_z);
		setElementSize(txt_reg_z);
		gbp_reg_txt.add2Column(txt_reg_z);
		gbp_reg_txt.nextRow();
		setElementSize(lbl_reg_uMin);
		gbp_reg_txt.add2Column(lbl_reg_uMin);
		setElementSize(txt_reg_uMin);
		gbp_reg_txt.add2Column(txt_reg_uMin);
		gbp_reg_txt.nextRow();
		setElementSize(lbl_reg_uMax);
		gbp_reg_txt.add2Column(lbl_reg_uMax);
		setElementSize(txt_reg_uMax);
		gbp_reg_txt.add2Column(txt_reg_uMax);
		gbp_reg_txt.nextRow();
		setElementSize(lbl_reg_t);
		gbp_reg_txt.add2Column(lbl_reg_t);
		setElementSize(txt_reg_t);
		gbp_reg_txt.add2Column(txt_reg_t);

		/*
		 * Pannelgerueste zusammenbauen
		 */
		gbp_main.setWeightFill(1, 0.5);
		gbp_main.add2Column(gbp_sys);
		gbp_par.setMinimumSize(new Dimension(130, 210));
		gbp_main.add2Column(gbp_par);
		gbp_main.nextRow();
		gbp_main.setAlign("right");
		gbp_main.add2Column(cb_main_reg, 2);
		gbp_main.nextRow();
		gbp_main.setWeightFill(1, 0.5);
		gbp_main.add2Column(gbp_ein);
		gbp_main.nextRow();
		gbp_main.setWeightFill(1, 0.5);
		gbp_main.add2Column(gbp_reg, 2);
		gbp_reg.setWeightFill(1);
		gbp_reg.add2Column(gbp_reg_sld);
		gbp_reg_txt.setMinimumSize(new Dimension(130, 120));
		gbp_reg.add2Column(gbp_reg_txt);
		gbp_reg.setVisible(false);
		gbp_main.nextRow();
		gbp_main.setAlign("right");
		gbp_main.add2Column(but_main_start, 2);

		/*
		 * Fenster
		 */
		this.add(gbp_main);
		this.setTitle("Systeme/Signale - Simulation");
		this.setVisible(true);
		this.pack();
		this.setMinimumSize(new Dimension(500, 525));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void setElementSize(JScrollPane el) {
		// TODO Auto-generated method stub
		el.setPreferredSize(new Dimension(75, 80));
	}

	private void setElementSize(JTextField el) {
		el.setPreferredSize(new Dimension(75, 20));
	}

	private void setElementSize(JLabel el) {
		el.setPreferredSize(new Dimension(35, 20));
	}

	/**
	 * Setzt den Inputlistener fuer Textfelder
	 * 
	 * @param txtFeld
	 */
	private void setCheckEingabe(JTextField txtFeld) {
		txtFeld.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent arg0) {
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				JTextField el = (JTextField) arg0.getSource();
				int sel = el.getSelectionStart();
				el.setText(checkDoubleTxt(el.getText())); // Text wird direkt
															// geprueft
				el.setSelectionStart(sel);
				el.setSelectionEnd(sel);
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				JTextField el = (JTextField) arg0.getSource();
				if (el.getText().equals(DOUBLE_NEUTRAL)) {
					el.setText("");
				}
			}
		});
		txtFeld.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				JTextField el = (JTextField) e.getSource();
				String txt = el.getText();
				txt.replace(",", ".");
				el.setText(txt);
			}

			@Override
			public void focusGained(FocusEvent e) {
			}
		});
	}

	private void setCheckEingabe(JTextField txtFeld, String typ) {
		if (typ.equals("int")) {
			txtFeld.addKeyListener(new KeyListener() {
				@Override
				public void keyTyped(KeyEvent arg0) {
				}

				@Override
				public void keyReleased(KeyEvent arg0) {
					JTextField el = (JTextField) arg0.getSource();
					int sel = el.getSelectionStart();
					el.setText(checkIntegerTxt(el.getText())); // Text wird
																// direkt
																// geprueft
					el.setSelectionStart(sel);
					el.setSelectionEnd(sel);
				}

				@Override
				public void keyPressed(KeyEvent arg0) {
					JTextField el = (JTextField) arg0.getSource();
					if (el.getText().equals(INTEGER_NEUTRAL)) {
						el.setText("");
					}
				}
			});
		}
	}

	/**
	 * Setzt den Inputlistener fuer Textareas
	 * 
	 * @param txtArea
	 */
	private void setCheckEingabe(JTextArea txtArea) {
		txtArea.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent arg0) {
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				JTextArea el = (JTextArea) arg0.getSource();
				int sel = el.getSelectionStart();
				el.setText(readLines(el.getText())); // Text wird in zerlegt
				el.setSelectionStart(sel);
				el.setSelectionEnd(sel);
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				JTextArea el = (JTextArea) arg0.getSource();
				if (el.getText().equals(DOUBLE_NEUTRAL)) {
					el.setText("");
				}
			}
		});
		txtArea.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				JTextArea el = (JTextArea) e.getSource();
				el.setText(el.getText().replace(",", "."));
			}

			@Override
			public void focusGained(FocusEvent e) {
			}
		});
	}

	/**
	 * Wandelt mehrzeiligen Text in Array um und ueberprueft jeweils
	 * 
	 * @param txt
	 * @return
	 */
	private static String readLines(String txt) {
		txt += "#"; // Dummy Symbol - um tatsaechliche Zeilen Anzahl zu bekommen
		String[] txtLines = txt.split("\n");
		int size = txtLines.length;
		txtLines[size - 1] = txtLines[size - 1].substring(0,
				txtLines[size - 1].length() - 1);

		String txtNew = "";
		for (int i = 0; i < size; i++) {
			txtNew += ((i > 0) ? "\n" : "") + checkDoubleTxt(txtLines[i]);
		}
		return txtNew;
	}

	/**
	 * Prueft bei Eingabe ob String ein Double ist und korrigiert sie
	 * 
	 * @param txt
	 * @return
	 */
	private static String checkDoubleTxt(String txt) {
		String pattern = "[^0-9.,+-]*([+-]?)[^0-9.,]*(\\d+)[^0-9.,]*([.,]?)[^0-9]*(\\d*)[^0-9]*";
		txt = txt.replaceAll(pattern, "$1$2$3$4");
		return String.valueOf(checkDouble(txt));
	}

	/**
	 * Prueft den Text ob er ein Double ist
	 * 
	 * @param txt
	 * @return
	 */
	private static double checkDouble(String txt) {
		if (txt.matches("([+-]?)(\\d+)(([.,]?)(\\d+))?")) {
			return Double.valueOf(txt);
		}
		return DOUBLE_NEUTRAL;
	}

	/**
	 * Wandelt ein TextArea in einen Vector
	 * 
	 * @param el
	 * @return
	 */
	private double[] getDouble(JTextArea el) {
		String[] tmp_ar = el.getText().split("\n");
		double[] dbl_vec = new double[tmp_ar.length];

		for (int i = 0; i < tmp_ar.length; i++) {
			dbl_vec[i] = checkDouble(tmp_ar[i]);
		}

		return dbl_vec;
	}

	/**
	 * Wandelt ein TextField in einen Doublewert
	 * 
	 * @param el
	 * @return
	 */
	private double getDouble(JTextField el) {
		return checkDouble(el.getText());
	}

	/**
	 * Wandelt ein Label in einen Doublewert
	 * 
	 * @param el
	 * @return
	 */
	private double getDouble(JLabel el) {
		return checkDouble(el.getText());
	}

	/**
	 * Prueft bei Eingabe ob String ein Integer ist und korrigiert sie
	 * 
	 * @param txt
	 * @return
	 */
	private static String checkIntegerTxt(String txt) {
		String pattern = "[^\\d]*(\\d+)[^\\d]*";
		txt = txt.replaceAll(pattern, "$1");
		return String.valueOf(checkInteger(txt));
	}

	/**
	 * Prueft den Text ob er ein Integer ist
	 * 
	 * @param txt
	 * @return
	 */
	private static int checkInteger(String txt) {
		if (txt.matches("(\\d+)")) {
			// Maximaleingabe
			if (txt.length() >= MAX_INT_SIZE) {
				txt = txt.substring(0, MAX_INT_SIZE - 1);
			}
			return Integer.valueOf(txt);
		}
		return INTEGER_NEUTRAL;
	}

	/**
	 * Wandelt ein TextField in einen Integerwert
	 * 
	 * @param el
	 * @return
	 */
	private int getInteger(JTextField el) {
		return checkInteger(el.getText());
	}
}
