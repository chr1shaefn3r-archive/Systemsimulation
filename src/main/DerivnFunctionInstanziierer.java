package main;

import java.io.IOException;

public class DerivnFunctionInstanziierer extends Instanziierer {

	public DerivnFunctionInstanziierer() {
		super("RungenKutta", "DerivnFunction");
	}

	public DerivnFunction instanziiere(String pClass) throws IOException{
		return (DerivnFunction)super.instanziiere(pClass);
	}
}
