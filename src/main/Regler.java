package main;

public class Regler implements Regelbar {

	/**
	 * Attribute
	 */
	private double[] mAktuellerZustand;
	private double mYold;
	private double mU;
	private double mKi;
	private double mKd;
	private double mKp;
	private double mSoll;
	private double mStepSize;
	private double mUHigh;
	private double mULow;
	private double mIntegral;
	private double mDifference;

	public Regler(double pKp, double pKi, double pKd, double pSoll,
			double pStepsize, double pULow, double pUHigh) {
		this.mKi = pKi;
		this.mKd = pKd;
		this.mKp = pKp;
		this.mSoll = pSoll;
		this.mStepSize = pStepsize;
		this.mUHigh = pUHigh;
		this.mULow = pULow;
		this.mIntegral = 0;
		this.mDifference = 0;
	}

	@Override
	public void reglerInput(double[] pSystemzustand, double pZeit) {
		this.mAktuellerZustand = pSystemzustand;
		java.lang.System.out.println("[Regler] bekommt neuen SystemZustand: '"
				+ pSystemzustand[0] + "', bei (" + pZeit + ")");
		rechneReglerwert();
	}

	@Override
	public double[] reglerOutput() {
		double[] reglerOutput = new double[] { mU };
		return reglerOutput;
	}

	private void rechneReglerwert() {
		double h = mStepSize;
		double Tf = 1;
		double Tt = 1;
		double b = 1;
		// Precompute controller coefficients
		double bi = mKi * h;
		double ad = Tf / (Tf + h);
		double bd = mKd / (Tf + h);
		double br = h / Tt;
		// controll algorithm - main
		double r = mSoll; // read setpoint from ch1
		double y = this.mAktuellerZustand[0]; // read process variable from ch2
		double P = mKp * (b * r - y); // compute proportional part
		mDifference = ad * mDifference - bd * (y - mYold); // update derivate
															// part
		double v = P + mIntegral + mDifference; // compute temporary output
		double u = 0;
		u = sat(v, mULow, mUHigh);
		mU = u;
		mIntegral = mIntegral + bi * (r - y) + br * (u - v); // update integral
		mYold = y; // update old process output
	}

	private double sat(double pV, double pUlow, double pUhigh) {
		if (pV <= pUhigh && pV >= pUlow) {
			return pV;
		} else {
			if (pV > pUhigh)
				return pUhigh;
			else
				return pUlow;
		}
	}
}
